package com.example.networkexample.model

data class Person(
    val id: Int,
    val name: String,
    val height: Int,
)