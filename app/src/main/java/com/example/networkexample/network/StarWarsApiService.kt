package com.example.networkexample.network

import retrofit2.http.GET
import retrofit2.http.Path

interface StarWarsApiService {

    @GET("people")
    suspend fun getPeople(): PeopleResponse

    @GET("people/{id}")
    suspend fun getPerson(
        @Path("id") id: Int,
    ): PersonDetailsResponse
}