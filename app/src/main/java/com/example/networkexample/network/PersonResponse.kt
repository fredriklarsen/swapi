package com.example.networkexample.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonResponse(
    val name: String,
    val height: Int,
    val url: String,
)