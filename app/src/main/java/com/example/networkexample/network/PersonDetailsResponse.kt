package com.example.networkexample.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonDetailsResponse(
    val name: String,
    val height: Int,
    val url: String,
    val mass: String,
    val films: List<String>,
    val hair_color: String, // Has to exactly match json ke!
    @Json(name = "eye_color") val eyeColor: String, // Unless you specify explicitly
    @Json(name = "birth_year") val birthYear: String,
    val gender: String,
)