package com.example.networkexample.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PeopleResponse(
    val count: Int,
    val results: List<PersonResponse>,
)