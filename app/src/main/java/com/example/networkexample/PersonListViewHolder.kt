package com.example.networkexample

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PersonListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var name: TextView = itemView.findViewById(R.id.name)
    var height: TextView = itemView.findViewById(R.id.height)
}