package com.example.networkexample

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.networkexample.model.Person

class PersonAdapter(
    val onItemClick: (item: Person) -> Unit
) : ListAdapter<Person, PersonListViewHolder>(
    object : DiffUtil.ItemCallback<Person>() {
        override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem == newItem
        }

    }
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_person, parent,false)
        return PersonListViewHolder(view)
    }

    override fun onBindViewHolder(holder: PersonListViewHolder, position: Int) {
        val item = getItem(position)
        holder.name.text = item.name
        holder.height.text = item.height.toString()
        holder.itemView.setOnClickListener { onItemClick(item) }
    }

}