package com.example.networkexample

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.networkexample.model.Person
import com.example.networkexample.network.PeopleResponse
import com.example.networkexample.network.StarWarsApiService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
        const val API_ENDPOINT = "https://swapi.dev/api/"
    }

    private lateinit var adapter: PersonAdapter

    private val retrofit = Retrofit
        .Builder()
        .baseUrl(API_ENDPOINT)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    private val apiService = retrofit.create(StarWarsApiService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = PersonAdapter(onItemClick)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        lifecycleScope.launchWhenCreated {
            val peopleResponse = apiService.getPeople()

            val people = mapToListOfPerson(peopleResponse)

            adapter.submitList(people)

            Log.d(TAG, "There are ${peopleResponse.count} items available")
        }

    }

    private val onItemClick: (person: Person) -> Unit = { person ->
        lifecycleScope.launchWhenResumed {
            val response = apiService.getPerson(person.id)

            val pronounPlural = when (response.gender) {
                "male" -> "His"
                "female" -> "Her"
                else -> "Their"
            }

            AlertDialog.Builder(this@MainActivity)
                .setTitle(response.name)
                .setMessage("""
                        • Appears in ${response.films.count()} film(s)!
                        
                        • $pronounPlural eye color is ${response.eyeColor} and ${pronounPlural.lowercase()} hair color is ${response.hair_color}
                        
                        • Born in ${response.birthYear} and grew up to be ${person.height}cm tall.
                        
                        • Weighs ${response.mass}kg on Earth
                    """.trimIndent())
                .show()
        }
    }


    private fun mapToListOfPerson(peopleResponse: PeopleResponse) =
        peopleResponse.results.map { response ->
            // The id of the item is the last path segment
            // E.g. "https://swapi.dev/api/people/1"
            val lastPath = response.url.toUri().lastPathSegment ?: error("Missing id!")

            Person(
                id = lastPath.toInt(),
                name = response.name,
                height = response.height
            )
        }

}


